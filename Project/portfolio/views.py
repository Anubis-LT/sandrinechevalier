from .models import Portfolio
from console_admin.models import Parameters
from django.shortcuts import render


# # Create your views here.
def allportfolio(request):
    # query the db to return all projects objects
    portfolios = Portfolio.objects.all()
    context = {'portfolios': portfolios}
    params = Parameters.objects.all()

    for param in params:
        if param.keyfield == 'logo_societe':
            context['logo'] = param.imagefield
        if param.keyfield == 'tel_societe':
            context['tel_societe'] = param.charfield
    return render(request, 'portfolio/list.html', context)


def detailportfolio(request, pk):
    portfolios = Portfolio.objects.get(id=pk)
    context = {'portfolios': portfolios}
    params = Parameters.objects.all()
    for param in params:
        if param.keyfield == 'logo_societe':
            context['logo'] = param.imagefield
        if param.keyfield == 'tel_societe':
            context['tel_societe'] = param.charfield

    return render(request, 'portfolio/detail.html', context)
