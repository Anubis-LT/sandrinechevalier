from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Portfolio(models.Model):
    """Custom User model definition for portfolio."""
    title = models.CharField(_('title'), max_length=30, blank=True)
    description = models.TextField(_('description'), blank=True)
    date_lastupdate = models.DateTimeField(_('date'), auto_now=True)
    img_before = models.ImageField(upload_to='images/')
    img_after = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.title
