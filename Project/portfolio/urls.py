from django.contrib.auth import views as auth_views
from django.urls import path
from . import views


app_name = 'portfolio'
urlpatterns = [
    path('all/', views.allportfolio,name='all'),
    path("detail/<int:pk>/", views.detailportfolio,name='detail'),
]
