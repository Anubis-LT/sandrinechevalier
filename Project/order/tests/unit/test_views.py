from django.test import Client, TestCase, RequestFactory
from django.urls import reverse

from authentication.models import Account
from order.models import Order,OrderItem
from order.views import CreateOrderView
client = Client()


class TestOrders(TestCase):
    fixtures = ['BddTest.json']

    @classmethod
    def setUpTestData(cls):
        user = Account.objects.create(email='test_admin@test.com', is_staff=True)
        user.set_password('123')
        user.save()

    def test_order(self):
        """ check page Create """
        client.login(username='test_admin@test.com', password="123")
        response = client.get("/dashboard/create/")
        self.assertEqual(response.status_code, 200)
        response = client.get(reverse("console_admin:create_order"))
        self.assertEqual(response.status_code, 200)


class TestCreateOrder(TestCase):
    fixtures = ['BddTest.json']

    @classmethod
    def setUpTestData(cls):
        client.login(username='test_admin@test.com', password="123")
        cls.user = Account.objects.create(email='test2@test.com', is_staff=True)
        cls.user.set_password('123')
        cls.user.save()


    def test_create_order_success(self):
        client.login(username='test_admin@test.com', password="123")
        self.assertEqual(Order.objects.all().count(), 0)
        order = Order.objects.create(customer_id=self.user.id,value=100)
        order.save()
        self.assertEqual(Order.objects.all().count(), 1)

        self.assertEqual(OrderItem.objects.all().count(), 0)
        orderitem = OrderItem.objects.create(order_id=order.id,product_id=1,qty=1)
        orderitem.save()
        self.assertEqual(OrderItem.objects.all().count(), 1)

    def test_get_absolute_url(self):
        client.login(username='test_admin@test.com', password="123")
        order = Order.objects.create(customer_id=self.user.id, value=100)
        order.save()
        # This will also fail if the urlconf is not defined.
        self.assertEquals(order.get_edit_url(), '/dashboard/update/2/')

