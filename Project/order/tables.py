import django_tables2 as tables

from .models import OrderItem, Order
from .models import Product


class OrderTable(tables.Table):
    tag_final_value = tables.Column(orderable=False, verbose_name='Value')
    action = tables.TemplateColumn(
        '<a href="{{ record.get_edit_url }}" class="btn btn-info"><i class="fa fa-edit"></i></a>', orderable=False)

    class Meta:
        model = Order
        template_name = 'django_tables2/bootstrap.html'
        fields = ['id', 'created_at', 'title', 'customer', 'is_paid', 'tag_final_value']


class OrderItemTable(tables.Table):
    tag_final_price = tables.Column(orderable=False, verbose_name='Price')
    action = tables.TemplateColumn('''
                {% if instance.is_paid == False %}
                    <a href="{% url "console_admin:modify_order_item" instance.id record.id  "add" %}" class="btn btn-success edit_button"><i class="fa fa-arrow-up"></i></a>
                    <a href="{% url "console_admin:modify_order_item" instance.id record.id "remove" %}" class="btn btn-warning edit_button"><i class="fa fa-arrow-down"></i></a>
                    <a href="{% url "console_admin:modify_order_item" instance.id record.id "delete" %}" class="btn btn-danger edit_button"><i class="fa fa-trash"></i></a>
                {% endif %}    
        ''', orderable=False)

    class Meta:
        model = OrderItem
        template_name = 'django_tables2/bootstrap.html'
        fields = ['product', 'qty', 'tag_final_price']


class ProductOrderTable(tables.Table):
    tag_final_value = tables.Column(orderable=False, verbose_name='Price')
    action = tables.TemplateColumn('''
        <a href="{% url "console_admin:add_product" instance.id record.id %}" class="btn btn-info">Ajouter l'article</a>
        ''', orderable=False)

    class Meta:
        model = Product
        template_name = 'django_tables2/bootstrap.html'
        fields = ['title', 'category', 'tag_final_value']
