import datetime

from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import Sum
from django.shortcuts import get_object_or_404, reverse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView
from django.views.generic import ListView
from django_tables2 import RequestConfig

from authentication.models import Account
from order.forms import OrderCreateForm, OrderEditForm
from order.models import Order, CURRENCY
from order.models import OrderItem
from order.tables import OrderItemTable, ProductOrderTable
from order.tables import OrderTable
from product.models import Product
from console_admin.models import Parameters


class HomepageView(ListView):
    """ List orders"""
    template_name = 'dashboard_admin.html'
    model = Order
    queryset = Order.objects.all()[:10]

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        orders = Order.objects.all()
        count_customers = Account.objects.filter(is_staff=False).count()

        # Qty Order
        quotation_count = orders.filter(quotation=True).count()
        order_no_is_paid_count = orders.filter(quotation=False, is_paid=False).count()
        order_is_paid_count = orders.filter(is_paid=True).count()

        # Mount
        total_quotation = orders.filter(quotation=True).aggregate(Sum('final_value'))['final_value__sum'] \
            if orders.filter(quotation=True).exists() else 0

        total_order_no_is_paid = orders.filter(quotation=False, is_paid=False).aggregate(Sum('final_value'))[
            'final_value__sum'] \
            if orders.filter(quotation=False, is_paid=False).exists() else 0

        total_sales = orders.aggregate(Sum('final_value'))['final_value__sum'] if orders.exists() else 0
        paid_value = orders.filter(is_paid=True).aggregate(Sum('final_value'))['final_value__sum'] \
            if orders.filter(is_paid=True).exists() else 0

        if order_is_paid_count > 0:
            average_order = round((total_sales / order_is_paid_count), 1)
        else:
            average_order = 0

        # Display
        average_order = f'{average_order} {CURRENCY}'
        total_quotation = f'{total_quotation} {CURRENCY}'
        total_order_no_is_paid = f'{total_order_no_is_paid} {CURRENCY}'
        total_sales = f'{total_sales} {CURRENCY}'
        paid_value = f'{paid_value} {CURRENCY}'
        orders = OrderTable(orders)
        RequestConfig(self.request).configure(orders)
        params = Parameters.objects.all()
        for param in params:
            if param.keyfield == 'logo_societe':
                logo = param.imagefield
            elif param.keyfield == 'tel_societe':
                tel_societe = param.charfield
        context.update(locals())
        return context


@method_decorator(staff_member_required, name='dispatch')
class CreateOrderView(CreateView):
    template_name = 'order/order_create.html'
    form_class = OrderCreateForm
    model = Order

    def get_success_url(self):
        return reverse('console_admin:update_order', kwargs={'pk': self.new_object.id})

    def form_valid(self, form):
        object = form.save()

        object.refresh_from_db()
        self.new_object = object
        return super().form_valid(form)


@method_decorator(staff_member_required, name='dispatch')
class OrderUpdateView(UpdateView):
    model = Order
    template_name = 'order/order_update.html'
    form_class = OrderEditForm

    def get_success_url(self):
        return reverse('console_admin:update_order', kwargs={'pk': self.object.id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        instance = self.object

        if instance.quotation == True and instance.is_paid == True:
            instance.quotation = False
            instance.save()

        qs_p = Product.objects.filter(active=True)[:12]
        products = ProductOrderTable(qs_p)
        order_items = OrderItemTable(instance.order_items.all())
        RequestConfig(self.request).configure(products)
        RequestConfig(self.request).configure(order_items)
        context.update(locals())
        return context


@method_decorator(staff_member_required, name='dispatch')
class OrderUpdateHeader(UpdateView):
    model = Order
    template_name = 'order/order_header_update.html'
    form_class = OrderEditForm

    def get_success_url(self):
        return reverse('console_admin:update_order', kwargs={'pk': self.object.id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        instance = self.object

        context.update(locals())
        return context


@staff_member_required
def delete_order(request, pk):
    instance = get_object_or_404(Order, id=pk)
    instance.delete()
    messages.warning(request, 'The order is deleted!')
    return redirect(reverse('admin:console_admin'))


@staff_member_required
def done_order_view(request, pk):
    instance = get_object_or_404(Order, id=pk)
    instance.save()
    return redirect(reverse('admin:console_admin'))


@staff_member_required
def add_product(request, pk, dk):
    instance = get_object_or_404(Order, id=pk)
    product = get_object_or_404(Product, id=dk)
    order_item, created = OrderItem.objects.get_or_create(order=instance, product=product)
    if created:
        order_item.qty = 1
        order_item.price = product.value
        order_item.discount_price = product.discount_value
    else:
        order_item.qty += 1
    order_item.save()
    return redirect(reverse('console_admin:update_order', kwargs={'pk': pk}))


@staff_member_required
def modify_order_item(request, orderid, pk, action):
    order = get_object_or_404(Order, id=orderid)
    order_item = get_object_or_404(OrderItem, id=pk)
    instance = order_item.order
    if action == 'add':
        order_item.qty += 1
    elif action == 'remove':
        order_item.qty -= 1
        order_item.qty = max(order_item.qty, 1)
    order_item.save()
    if action == 'delete':
        order_item.delete()
    instance.refresh_from_db()
    order_items = OrderItemTable(instance.order_items.all())
    RequestConfig(request).configure(order_items)
    return redirect(reverse('console_admin:update_order', kwargs={'pk': order.id}))


@staff_member_required
def order_action_view(request, pk, action):
    instance = get_object_or_404(Order, id=pk)
    if action == 'delete':
        instance.delete()
    elif action == 'is_paid':
        instance.is_paid = True
        """ paid =True we force the value quotation a False """
        instance.quotation = False
        instance.save()
    return redirect(reverse('admin:console_admin'))



