from django.views.generic.base import TemplateView
from console_admin.models import Parameters


class HomePageViewIndex(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        params = Parameters.objects.all()
        context = super().get_context_data(**kwargs)
        for param in params:
            if param.keyfield == 'logo_societe':
                context['logo'] = param.imagefield
            if param.keyfield == 'nom_societe':
                context['nom_societe'] = param.charfield
            if param.keyfield == 'adresse_societe':
                context['adresse_societe'] = param.charfield
            if param.keyfield == 'tel_societe':
                context['tel_societe'] = param.charfield
            if param.keyfield == 'email_societe':
                context['email_societe'] = param.charfield
            if param.keyfield == 'texte_accroche':
                context['texte_accroche'] = param.charfield
            if param.keyfield == 'texte_accroche_complement':
                context['texte_accroche_complement'] = param.charfield
            if param.keyfield == 'propos_moi':
                context['propos_moi'] = param.textfield

        return context
