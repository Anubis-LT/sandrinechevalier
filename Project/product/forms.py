from django import forms
from .models import Category, Product


class BaseForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class CategoryCreateForm(BaseForm, forms.ModelForm):
    class Meta:
        model = Category
        fields = ['title']


class CategoryEditForm(BaseForm, forms.ModelForm):
    class Meta:
        model = Category
        fields = ['title']


class ProductCreateForm(BaseForm, forms.ModelForm):
    class Meta:
        model = Product
        fields = ['title','category','qty','value','active']


class ProductEditForm(BaseForm, forms.ModelForm):
    class Meta:
        model = Product
        fields = ['title','category','qty','value','active']
