import django_tables2 as tables
from .models import Category, Product


class CategoryTable(tables.Table):
    acces = tables.TemplateColumn('''          
            <a href="{% url 'console_admin:categories_update' record.id %}" class="btn btn-info"><i class="fa fa-edit"></i></a>
            <a href="{% url 'console_admin:categories_delete' record.id %}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
        ''', orderable=False)

    class Meta:
        model = Category
        template_name = 'django_tables2/bootstrap.html'
        fields = ['id', 'title']


class ProductTable(tables.Table):
    acces = tables.TemplateColumn('''        
               <a href="{% url 'console_admin:product_update' record.id %}" class="btn btn-info"><i class="fa fa-edit"></i></a>
               <a href="{% url 'console_admin:product_delete' record.id %}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
            ''', orderable=False)

    class Meta:
        model = Product
        template_name = 'django_tables2/bootstrap.html'
        fields = ['title', 'category', 'tag_final_value']
