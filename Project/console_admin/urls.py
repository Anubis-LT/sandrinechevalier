from django.urls import path
from .views import customer, parameters,product, portfolio
from order.views import (HomepageView, OrderUpdateView, CreateOrderView, delete_order,
                         done_order_view, OrderUpdateHeader,
                         add_product, modify_order_item,  order_action_view
                         )

app_name = 'console_admin'
urlpatterns = [
    path('', HomepageView.as_view(), name='console_admin'),

    # Customers
    path('list_customer', customer.list_customer, name='list_customer'),
    path('create_customer/', customer.customer, name="create_customer"),
    path('update_customer/<str:pk>/', customer.customer, name="update_customer"),
    path('delete_customer/<str:pk>/', customer.delete_customer,
         name="delete_customer"),

    # Parameters
    path('list_parameters/', parameters.list_parameters, name="list_parameters"),
    path('create_parameters/', parameters.parameters, name="create_parameters"),
    path('update_parameters/<str:pk>/', parameters.parameters, name="update_parameters"),
    path('delete_parameters/<str:pk>/', parameters.delete_parameters, name="delete_parameters"),


    # Portfolio
    path('list_portfolio/', portfolio.list_portfolio, name="list_portfolio"),
    path('create_portfolio/', portfolio.create_portfolio, name="create_portfolio"),
    path('update_portfolio/<str:pk>/', portfolio.update_portfolio,
         name="update_portfolio"),
    path('delete_portfolio/<str:pk>/', portfolio.delete_portfolio,
         name="delete_portfolio"),

    # Category
    path('categories-list/', product.CategoriesListView.as_view(), name='categories_list'),
    path('categories-create/', product.CreateCategoriesView.as_view(), name='categories_create'),
    path('categories-update/<int:pk>/', product.CategoryUpdateView.as_view(), name='categories_update'),
    path('categories-delete/<int:pk>/', product.delete_category, name='categories_delete'),

    # Product
    path('product-list/', product.ProductListView.as_view(), name='product_list'),
    path('product-create/', product.CreateProductView.as_view(), name='product_create'),
    path('product-update/<int:pk>/', product.ProductUpdateView.as_view(), name='product_update'),
    path('product-delete/<int:pk>/', product.delete_product, name='product_delete'),

    # order
    path('create/', CreateOrderView.as_view(), name='create_order'),
    path('update-order/<int:pk>/', OrderUpdateHeader.as_view(), name='update_order_header'),
    path('update/<int:pk>/', OrderUpdateView.as_view(), name='update_order'),
    path('done/<int:pk>/', done_order_view, name='done_order'),
    path('delete/<int:pk>/', delete_order, name='delete_order'),
    path('action/<int:pk>/<slug:action>/', order_action_view, name='order_action'),
    path('add-product/<int:pk>/<int:dk>/',add_product, name='add_product'),
    path('modify-order-item/<int:orderid>/<int:pk>/<slug:action>', modify_order_item, name='modify_order_item'),

]
