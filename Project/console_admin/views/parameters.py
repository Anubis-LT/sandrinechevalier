from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.shortcuts import render

from console_admin.forms.parameters import ParametersForm
from console_admin.models import Parameters
from authentication.models import Account
from order.models import Order


@staff_member_required()
@login_required(login_url="index")
def list_parameters(request):
    """ list Parameters """
    params = Parameters.objects.all()
    return render(request, 'console_admin/parameters_list.html', locals())


@staff_member_required()
@login_required(login_url="index")
def parameters(request, pk=None):
    """ Creaate or Update Parameters"""
    if pk is None:
        # Create Parameters
        if request.method == "POST":
            param_form = ParametersForm(request.POST)
            param_form.fields['keyfield'].widget.attrs['readonly'] = True
            if param_form.is_valid():
                params = param_form.save()
                params.save()
                return redirect('console_admin:list_parameters')
        else:
            param_form = ParametersForm()
            param_form.fields['keyfield'].widget.attrs['readonly'] = True

        return render(request, 'console_admin/parameters_form.html',
                      {'param_form': param_form})
    else:
        # Update Parameters
        params = Parameters.objects.get(id=pk)
        param_form = ParametersForm(instance=params)

        if request.method == "POST":
            param_form = ParametersForm(request.POST, request.FILES,
                                        instance=params)
            if param_form.is_valid():
                param_form.save()
                return redirect('console_admin:list_parameters')

        context = {
            'param_form': param_form}

        return render(request, 'console_admin/parameters_form.html', context)


@staff_member_required()
@login_required(login_url="index")
def delete_parameters(request, pk=None):
    """ Delete parameters with the check question """
    params = Parameters.objects.get(id=pk)
    if request.method == "POST":
        params.delete()
        return redirect('console_admin:list_parameters')
    context = {'item': params}
    return render(request, 'console_admin/delete.html', context)
