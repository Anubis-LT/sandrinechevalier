from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required

from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.core.mail import send_mail

from authentication.models import Account, Address
from console_admin.forms.account import AccountForm, AddressForm
from order.models import Order

"""
    List, create, update, delete customer and address
"""


@staff_member_required()
@login_required(login_url="index")
def list_customer(request):
    """List customer"""
    customers = Account.objects.filter(is_staff=False).order_by('id')
    count_customers = Account.objects.filter(is_staff=False).count()
    return render(request, 'console_admin/customer_list.html', locals())


@staff_member_required()
@login_required(login_url="index")
def customer(request, pk=None):
    " Create or Update customer"
    inst_user = Account.objects.get(id=pk) if pk is not None else None
    inst_address_billing = Address.objects.filter(users_id=pk) if pk is not None else None

    if request.method == "GET":
        account_form = AccountForm(instance=inst_user)
        address_form = AddressForm(instance=inst_address_billing)

        return render(request, 'console_admin/customer_form.html', locals())
    else:
        account_form = AccountForm(request.POST, request.FILES, instance=inst_user)
        address_form = AddressForm(request.POST, request.FILES, instance=inst_address_billing)

        if account_form.is_valid() and address_form.is_valid():

            if pk is not None:
                account_form.save()
                address_form.save()
            else:
                my_password = Account.objects.make_random_password()
                account = account_form.save(commit=False)
                account.set_password(my_password)
                account.save()

                address = address_form.save(commit=False)

                # Assign Account to Address billing
                address.users = account
                address.save()

                # Send an email to the user with the token:
                mail_subject: str = '[Sandrine Chevalier] Votre compte'
                to_email: str = account.email
                context = {'account': account, 'passwd': my_password}
                msg_plain = render_to_string(
                    'console_admin/mail/account_create_update_passwd.txt',
                    context)
                msg_html = render_to_string(
                    'console_admin/mail/account_create_update_passwd.html',
                    context)
                send_mail(
                    mail_subject,
                    msg_plain,
                    'sc@gmail.com',
                    [to_email],
                    html_message=msg_html,
                )
            return redirect('console_admin:list_customer')


@staff_member_required()
@login_required(login_url="index")
def delete_customer(request, pk):
    """ Delete account customer with the check question """
    custom = Account.objects.get(id=pk)
    address = Address.objects.filter(users_id=pk)
    nborder = Order.objects.filter(customer_id=pk).count()

    if request.method == "POST":
        custom.delete()
        address.delete()
        return redirect('console_admin:list_customer')

    if nborder == 0:
        context = {'item': customer}
        return render(request, 'console_admin/delete.html', context)
    else:
        context = {'item': customer,
                   'message': 'Impossible de supprimer car commande en cours'
                   }
        return render(request, 'console_admin/delete.html', context)
