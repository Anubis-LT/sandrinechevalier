from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required

from portfolio.models import Portfolio
from portfolio.forms import PortfolioForm


@staff_member_required()
@login_required(login_url="index")
def list_portfolio(request):
    """ List porfolio"""
    portfolios = Portfolio.objects.all()
    return render(request, 'console_admin/portfolio_list.html', locals())


@staff_member_required()
@login_required(login_url="index")
def create_portfolio(request):
    """ Create porfolio"""
    form = PortfolioForm()

    if request.method == "POST":
        form = PortfolioForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect('console_admin:list_portfolio')

    context = {'form': form}
    return render(request, 'console_admin/portfolio_form.html', context)


@staff_member_required()
@login_required(login_url="index")
def update_portfolio(request, pk):
    """ Update porfolio"""
    portfolio = Portfolio.objects.get(id=pk)
    form = PortfolioForm(instance=portfolio)

    if request.method == "POST":
        form = PortfolioForm(request.POST, request.FILES, instance=portfolio)
        if form.is_valid():
            form.save()
        return redirect('console_admin:list_portfolio')

    context = {'form': form}
    return render(request, 'console_admin/portfolio_form.html', context)


@staff_member_required()
@login_required(login_url="index")
def delete_portfolio(request, pk):
    """ Delete portfolio with the check question """
    portfolio = Portfolio.objects.get(id=pk)

    if request.method == "POST":
        portfolio.delete()
        return redirect('console_admin:list_portfolio')

    context = {'item': portfolio}
    return render(request, 'console_admin/delete.html', context)
