from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import get_object_or_404, reverse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView
from django.views.generic import ListView
from django_tables2 import RequestConfig

from product.forms import CategoryCreateForm, CategoryEditForm, ProductCreateForm, ProductEditForm
from product.models import Category, Product
from product.tables import CategoryTable, ProductTable


@method_decorator(staff_member_required, name='dispatch')
class CategoriesListView(ListView):
    """ List categories """
    template_name = 'product/categories_list.html'
    model = Category
    paginate_by = 50

    def get_queryset(self):
        qs = Category.objects.all()
        if self.request.GET:
            qs = Category.filter_data(self.request, qs)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = CategoryTable(self.object_list)
        RequestConfig(self.request).configure(category)
        context.update(locals())
        return context


@method_decorator(staff_member_required, name='dispatch')
class CreateCategoriesView(CreateView):
    """ Create categories """
    template_name = 'product/categories_create.html'
    form_class = CategoryCreateForm
    model = Category

    def get_success_url(self):
        self.new_object.refresh_from_db()
        return reverse('console_admin:categories_list')

    def form_valid(self, form):
        object = form.save()
        object.refresh_from_db()
        self.new_object = object
        return super().form_valid(form)


@method_decorator(staff_member_required, name='dispatch')
class CategoryUpdateView(UpdateView):
    """ Update categories """
    model = Category
    template_name = 'product/categories_update.html'
    form_class = CategoryEditForm

    def get_success_url(self):
        return reverse('console_admin:categories_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        instance = self.object
        qs_p = Category.objects.filter()[:12]
        category = CategoryTable(qs_p)
        RequestConfig(self.request).configure(category)
        context.update(locals())
        return context


@staff_member_required
def delete_category(request, pk):
    """ Delete categories """
    instance = get_object_or_404(Category, id=pk)
    instance.delete()
    messages.warning(request, 'La categorie a été supprimé')
    return redirect(reverse('console_admin:categories_list'))


# Product
@method_decorator(staff_member_required, name='dispatch')
class ProductListView(ListView):
    """ List categories """
    template_name = 'product/product_list.html'
    model = Product
    paginate_by = 50

    def get_queryset(self):
        qs = Product.objects.all()
        if self.request.GET:
            qs = Product.filter_data(self.request, qs)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        product = ProductTable(self.object_list)
        RequestConfig(self.request).configure(product)
        context.update(locals())
        return context


@method_decorator(staff_member_required, name='dispatch')
class CreateProductView(CreateView):
    template_name = 'product/product_create.html'
    form_class = ProductCreateForm
    model = Product

    def get_success_url(self):
        self.new_object.refresh_from_db()
        return reverse('console_admin:product_list')

    def form_valid(self, form):
        object = form.save()
        object.refresh_from_db()
        self.new_object = object
        return super().form_valid(form)


@method_decorator(staff_member_required, name='dispatch')
class ProductUpdateView(UpdateView):
    model = Product
    template_name = 'product/product_update.html'
    form_class = ProductEditForm

    def get_success_url(self):
        return reverse('console_admin:product_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        instance = self.object
        qs_p = Product.objects.filter()[:12]
        product = ProductTable(qs_p)
        RequestConfig(self.request).configure(product)
        context.update(locals())
        return context


@staff_member_required
def delete_product(request, pk):
    instance = get_object_or_404(Product, id=pk)
    instance.delete()
    messages.warning(request, 'Le produit a été supprimé')
    return redirect(reverse('console_admin:product_list'))
