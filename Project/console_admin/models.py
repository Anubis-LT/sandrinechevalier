from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.
# Parameters site
class Parameters(models.Model):
    keyfield = models.CharField(_('key'),unique=True,max_length=50)
    keydescription = models.TextField(_('descriptions'))
    charfield = models.CharField(_('char'), max_length=50,blank=True)
    textfield = models.TextField(_('text'),blank=True)
    decimalfield = models.DecimalField(_('decimal'), max_digits=8, decimal_places=2,default=0)
    intergerfield = models.IntegerField(_('integer'),default=0)
    booleanfield = models.BooleanField(_('boolean'),default=False)
    imagefield = models.ImageField(upload_to='param_image',blank=True)
    reserved_admin = models.BooleanField(_('boolean'),default=False)

    class Meta:
        verbose_name = 'keyfield'

        indexes = [
            models.Index(fields=['keyfield']),
        ]
