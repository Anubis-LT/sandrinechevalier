from django.test import Client, TestCase
from django.urls import reverse

from authentication.models import Account
from console_admin.models import Parameters
from portfolio.models import Portfolio

client = Client()


class TestProducts(TestCase):
    fixtures = ['BddTest.json']

    @classmethod
    def setUpTestData(cls):
        user = Account.objects.create(email='test_admin@test.com', is_staff=True)
        user.set_password('123')
        user.save()

    def test_dashboard(self):
        """ check page admin """
        admin_pages = {
            "/dashboard/": 200,
            "/dashboard/list_customer": 200,
            "/dashboard/create_customer": 301,
            "/dashboard/list_parameters": 301,
            "/dashboard/categories-list": 301,
            "/dashboard/product-list": 301,
            "/dashboard/list_portfolio": 301
        }
        # log admin
        client.login(username='test_admin@test.com', password="123")
        for p, s in admin_pages.items():
            response = client.get(p)
            self.assertEqual(response.status_code, s)


class TestDetailCategory(TestCase):
    fixtures = ['BddTest.json']

    @classmethod
    def setUpTestData(cls):
        user = Account.objects.create(email='test_admin@test.com', is_staff=True)
        user.set_password('123')
        user.save()

    def test_detail_success(self):
        """ Test Category detail success"""
        client.login(username='test_admin@test.com', password="123")
        response = client.get('/dashboard/categories-list')
        self.assertEqual(response.status_code, 301)
        response = client.get(reverse('console_admin:categories_update', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        """ Test Product delete """
        client.login(username='test_admin@test.com', password="123")
        response = client.get(reverse('console_admin:categories_delete', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 302)

    def test_detail_fail(self):
        """ Test Category detail fail """
        client.login(username='test_admin@test.com', password="123")
        response = client.get(reverse('console_admin:categories_update', kwargs={'pk': 99}))
        self.assertEqual(response.status_code, 404)


class TestDetailProducts(TestCase):
    fixtures = ['BddTest.json']

    @classmethod
    def setUpTestData(cls):
        user = Account.objects.create(email='test_admin@test.com', is_staff=True)
        user.set_password('123')
        user.save()

    def test_detail_success(self):
        """ Test Product detail succes """
        client.login(username='test_admin@test.com', password="123")
        response = client.get(reverse('console_admin:product_update', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        """ Test Product delete """
        client.login(username='test_admin@test.com', password="123")
        response = client.get(reverse('console_admin:product_delete', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 302)

    def test_detail_fail(self):
        """ Test Product detail Fail """
        client.login(username='test_admin@test.com', password="123")
        response = client.get('/dashboard/product-update/99')
        self.assertEqual(response.status_code, 301)
        response = client.get(reverse('console_admin:product_update', kwargs={'pk': 99}))
        self.assertEqual(response.status_code, 404)

class TestPorfolio(TestCase):
    fixtures = ['BddTest.json']

    @classmethod
    def setUpTestData(cls):
        user = Account.objects.create(email='test_admin@test.com', is_staff=True)
        user.set_password('123')
        user.save()

    def test_detail_success(self):
        """ Test sur """
        client.login(username='test_admin@test.com', password="123")
        response = client.get('/dashboard/update_portfolio/1/')
        self.assertEqual(response.status_code, 200)
        response = client.get(reverse('console_admin:update_portfolio', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 200)

    def test_display_porfolio(self):
        """ Test display"""
        client.login(username='test_admin@test.com', password="123")
        response = client.get('/dashboard/list_portfolio/')
        self.assertEqual(response.status_code, 200)

    def test_create_porfolio(self):
        self.assertEqual(Portfolio.objects.all().count(), 1)
        req = Portfolio.objects.create(title="test", description="test description")
        req.save()
        self.assertEqual(Portfolio.objects.all().count(), 2)


class TestParameters(TestCase):
    fixtures = ['BddTest.json']

    @classmethod
    def setUpTestData(cls):
        user = Account.objects.create(email='test_admin@test.com', is_staff=True)
        user.set_password('123')
        user.save()

    def test_create_success(self):
        """ all goood: succes => modify db, user in request, redirect"""
        client.login(username='test_admin@test.com', password="123")
        self.assertEqual(Parameters.objects.all().count(), 2)
        reponse = client.post(
            reverse('console_admin:create_parameters'),
            {
                'keyfield': 'Test',
                'keydescription': 'Test sur les parametres',
                'decimalfield': 0,
                'intergerfield': 0,
                'reserved_admin': 1
            }
        )
        self.assertEqual(Parameters.objects.all().count(), 3)
        self.assertEqual(reponse.status_code, 302)

    def test_save_post_ok_exist(self):
        """Test all good but allready saved. """
        client.login(username='test_admin@test.com', password="123")
        self.assertEqual(Parameters.objects.all().count(), 2)
        response = client.post(
            reverse('console_admin:create_parameters'),
            {
                'keyfield': 'Test',
                'keydescription': 'Test sur les parametres',
                'decimalfield': 0,
                'intergerfield': 0,
                'reserved_admin': 1
            }
        )
        response = client.post(
            reverse('console_admin:create_parameters'),
            {
                'keyfield': 'Test',
                'keydescription': 'Test sur les parametres',
                'decimalfield': 0,
                'intergerfield': 0,
                'reserved_admin': 1
            }
        )
        self.assertEqual(Parameters.objects.all().count(), 3)
        self.assertEqual(response.status_code, 200)

    def test_update_parameter(self):
        """Test delete parameters. """
        client.login(username='test_admin@test.com', password="123")
        response = client.get(reverse('console_admin:update_parameters', kwargs={'pk': 2}))

        self.assertEqual(response.status_code, 200)

    def test_delete_post(self):
        """Test delete parameters. """
        client.login(username='test_admin@test.com', password="123")
        response = client.get(reverse('console_admin:delete_parameters', kwargs={'pk': 2}))
        self.assertEqual(response.status_code, 200)

    def test_fail(self):
        """Test Fail delete parameters. """
        client.login(username='test_admin@test.com', password="123")
        response = client.get('/update_parameters/999/')
        self.assertEqual(response.status_code, 404)


class TestCustomers(TestCase):
    fixtures = ['BddTest.json']

    @classmethod
    def setUpTestData(cls):
        user = Account.objects.create(email='test_admin@test.com', is_staff=True)
        user.set_password('123')
        user.save()





