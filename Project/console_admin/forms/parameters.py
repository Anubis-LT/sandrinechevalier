from django.forms import ModelForm
from console_admin.models import Parameters


class ParametersForm(ModelForm):
    class Meta:
        model = Parameters
        fields = ('keyfield',
                  'keydescription',
                  'charfield',
                  'textfield',
                  'decimalfield',
                  'intergerfield',
                  'booleanfield',
                  'imagefield')
