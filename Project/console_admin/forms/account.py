from django.forms import ModelForm
from authentication.models import Account, Address


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = (
            'last_name',
            'first_name',
            'email'
        )


class AddressForm(ModelForm):
    class Meta:
        model = Address
        fields = (
            'address',
            'address2',
            'zipcode',
            'city',
            'country',
            'phone1',
            'phone2'
        )
