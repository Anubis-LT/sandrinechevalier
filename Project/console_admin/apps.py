from django.apps import AppConfig


class ConsoleAdminConfig(AppConfig):
    name = 'console_admin'
