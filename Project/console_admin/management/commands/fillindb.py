from django.core.management.base import BaseCommand

from console_admin.models import Parameters


def save_in_db(param):
    for cles in param.values():
        p = Parameters(
            keyfield=cles['keyfield'],
            keydescription=cles['keydescription'],
            charfield=cles['charfield'],
            textfield=cles['textfield'],
            decimalfield=cles['decimalfield'],
            intergerfield=cles['intergerfield'],
            booleanfield=cles['booleanfield'],
            imagefield=cles['imagefield'],
            reserved_admin=True, )
        p.save()


class Command(BaseCommand):
    help = 'Fill in database with item default'

    var_field = [
        {
            "param1": {
                "keyfield": "nom_societe",
                "keydescription": "Nom de la societé",
                "charfield": "Sandrine Chevalier",
                "textfield": "",
                "decimalfield": 0,
                "intergerfield": False,
                "booleanfield": False,
                "imagefield": "",
            },
            "param2": {
                "keyfield": "adresse_societe",
                "keydescription": "Adresse de la societé",
                "charfield": "batard, 44260 Bouée, France",
                "textfield": "",
                "decimalfield": 0,
                "intergerfield": False,
                "booleanfield": False,
                "imagefield": "",
            },
            "param3": {
                "keyfield": "tel_societe",
                "keydescription": "Téléphone de la societé",
                "charfield": "06 08 68 65 82",
                "textfield": "",
                "decimalfield": 0,
                "intergerfield": False,
                "booleanfield": False,
                "imagefield": "",
            },
            "param4": {
                "keyfield": "logo_societe",
                "keydescription": "Logo  de la societé",
                "charfield": "",
                "textfield": "",
                "decimalfield": 0,
                "intergerfield": False,
                "booleanfield": False,
                "imagefield": "",
            },
            "param5": {
                "keyfield": "texte_accroche",
                "keydescription": "Version client, accroche sur la page d accueil",
                "charfield": "Tapissière d'ameublement",
                "textfield": "",
                "decimalfield": 0,
                "intergerfield": False,
                "booleanfield": False,
                "imagefield": "",
            },
            "param6": {
                "keyfield": "texte_accroche_complement",
                "keydescription": "Version client, suite accroche sur la page d accueil",
                "charfield": "Donnez un nouvel éclat à votre meuble !",
                "textfield": "",
                "decimalfield": 0,
                "intergerfield": False,
                "booleanfield": False,
                "imagefield": "",
            },
            "param7": {
                "keyfield": "propos_moi",
                "keydescription": "A propos de moi",
                "charfield": "",
                "textfield": "Passionnée d'anciens meuble, j'aime les restaurer avec soins, depuis des années, "
                             "j'affectionnes les tissus et l'histoire des mobiliers ...",
                "decimalfield": 0,
                "intergerfield": False,
                "booleanfield": False,
                "imagefield": "",
            },
            "param8": {
                "keyfield": "email_societe",
                "keydescription": "l'email par defaut de la société",
                "charfield": "contact@sandrinechevalier.com",
                "textfield": "",
                "decimalfield": 0,
                "intergerfield": False,
                "booleanfield": False,
                "imagefield": "",
            },
        }
    ]

    def handle(self, *args, **options):
        """ Delete & Save parameters """
        Parameters.objects.all().delete()
        self.stdout.write(self.style.SUCCESS("Done Init Parameters"))
        for param in self.var_field:
            save_in_db(param)

        self.stdout.write(self.style.SUCCESS("Done Import Parameters"))
