from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView

from console_admin.forms import AddressForm
from .forms import CustomerForm
from .forms import RegisterForm
from .models import Address


# Create your views here.
class AccountView(LoginRequiredMixin, TemplateView):
    """Show Account informations."""
    template_name = 'authentication/account.html'

    def get(self, request, *args, **kwargs):
        if not request.user.is_staff:
            pk = request.user.id

            # Check if address biling is an address completed
            address = Address.objects.get(users_id=pk, delivery_address=False)
            if address.zipcode == '':
                msg = "   Important, N' oubliez pas d'enregistrer votre adresse postale => "
                context = {
                    'customer': request.user,
                    'msgaddress': msg,
                }
            else:
                msg = 'ok'
                address_form = AddressForm(instance=address)
                context = {
                    'msgaddress': msg,
                    'customer': request.user,
                    'address_form': address_form

                }
        else:
            context = {}

        return render(request, self.template_name, context)


class InfoCustomer(LoginRequiredMixin, FormView):
    """Show Adress informations."""
    success_url = reverse_lazy('authentication:account')
    template_name = 'authentication/info_customer.html'

    def get(self, request, *args, **kwargs):
        pk = request.user.id

        # Check if address biling is an address completed
        address = Address.objects.get(users_id=pk, delivery_address=False)
        addressdelivery = Address.objects.get(users_id=pk,
                                              delivery_address=True)

        account_form = CustomerForm(instance=request.user)
        address_form = AddressForm(instance=address)

        if request.method == "POST":
            account_form = CustomerForm(request.POST, request.FILES,
                                        instance=request.user)
            address_form = AddressForm(request.POST, request.FILES,
                                       instance=address)

            if account_form.is_valid() and address_form.is_valid():
                account_form.save()
                address_form.save()
                return redirect('authentication:account')

        context = {
            'account_form': account_form,
            'address_form': address_form,
            'customer': request.user.id
        }

        return render(request, self.template_name, context)


class RegisterView(FormView):
    """Show Registration form and create user if valid."""
    form_class = RegisterForm
    success_url = reverse_lazy('authentication:account')
    template_name = 'authentication/register.html'

    def form_valid(self, form):
        form.save()

        email = form.cleaned_data.get('email')
        raw_password = form.cleaned_data.get('password1')
        user = authenticate(email=email, password=raw_password)
        if user:
            # Assign Account to Address delivery
            # Address biling
            ad = Address()
            ad.users = user
            ad.delivery_address = False
            ad.save()

            # Address Delivery
            ad1 = Address()
            ad1.users = user
            ad1.delivery_address = True
            ad1.save()

            login(self.request, user)
        return super().form_valid(form)


@login_required
def LogoutView(request):
    """Logout user."""
    logout(request)
    return redirect('index')
