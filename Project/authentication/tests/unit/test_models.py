from django.test import TestCase
from authentication.models import Account


class UserModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        user = Account.objects.create(email='test@test.com')
        user.set_password('12345')
        user.save()

    def test_first_name_label(self):
        user = Account.objects.get(id=1)
        field_label = user._meta.get_field('first_name').verbose_name
        self.assertEquals(field_label, 'prénom')

    def test_first_name_max_length(self):
        user = Account.objects.get(id=1)
        max_length = user._meta.get_field('first_name').max_length
        self.assertEquals(max_length, 30)

    def test_last_name_label(self):
        user = Account.objects.get(id=1)
        field_label = user._meta.get_field('last_name').verbose_name
        self.assertEquals(field_label, 'nom')

    def test_last_name_max_length(self):
        user = Account.objects.get(id=1)
        max_length = user._meta.get_field('last_name').max_length
        self.assertEquals(max_length, 30)
