from django.test import Client
from django.test import TestCase
from django.urls import reverse

from authentication.models import Account, Address

client = Client()


class TestAccount(TestCase):

    @classmethod
    def setUpTestData(cls):
        user = Account.objects.create(email='test@test.com')
        user.set_password('12345')
        user.save()
        address = Address.objects.create(users_id=user.id)
        address.save()

    def test_not_logged_redirect(self):
        # if not logged, redirect to login page
        response = client.get(reverse('authentication:account')).status_code
        self.assertEqual(response, 302)

    def test_loggedin_user(self):
        # if logged, show page
        client.login(email="test@test.com", password="12345")
        response = client.get(reverse('authentication:account')).status_code
        self.assertEqual(response, 200)


class TestLogin(TestCase):

    def test_login_url(self):
        response = client.get('/auth/login/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(reverse('authentication:login'), '/auth/login/')

    def test_login_success(self):
        """ test if login is ok and user in session."""
        reponse = client.post(
            reverse('authentication:login'),
            {'username': 'test@test.com', 'password': '12345'}
        )
        self.assertEqual(reponse.status_code, 200)

    def test_login_fail(self):
        """Test with wrong password, no connection."""
        reponse = client.post(
            reverse('authentication:login'),
            {'username': 'test@test.com', 'password': 'xxxxx'}
        )
        self.assertEqual(reponse.status_code, 200)


class TestLogout(TestCase):

    def test_not_logged_redirect(self):
        # if not logged, redirect to login page
        response = client.get(reverse('authentication:logout')).status_code
        self.assertEqual(response, 302)

    def test_loggedin_user(self):
        # test if user is logged
        # go to lougout page: redirection
        # user not legged anymore
        client.login(email="test@test.com", password="12345")
        response = client.get(reverse('authentication:logout')).status_code
        self.assertEqual(response, 302)


class TestRegister(TestCase):

    def test_register_url(self):
        response = client.get('/auth/register/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(reverse('authentication:register'), '/auth/register/')

    def test_register_success(self):
        """ all goood: succes => modify db, user in request, redirect"""
        self.assertEqual(Account.objects.all().count(), 0)
        reponse = client.post(
            reverse('authentication:register'),
            {
                'email': 'test@test.com',
                'password1': '12345###',
                'password2': '12345###'
            }
        )
        self.assertEqual(Account.objects.all().count(), 1)
        self.assertEqual(reponse.status_code, 302)
