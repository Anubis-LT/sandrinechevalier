from django.contrib.auth import views as auth_views
from django.urls import path
from . import views


app_name = 'authentication'
urlpatterns = [
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView, name='logout'),
    path('register/', views.RegisterView.as_view(), name='register'),
    path('account/', views.AccountView.as_view(), name='account'),
    path('account/infos/', views.InfoCustomer.as_view(), name='information'),

]
