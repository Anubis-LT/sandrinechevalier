from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm

from .models import Account, Address


class RegisterForm(UserCreationForm):
    """Register form definition."""

    class Meta:
        model = Account
        fields = (
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2')


class CustomerForm(ModelForm):
    """Register form definition."""

    class Meta:
        model = Account
        fields = (
            'first_name',
            'last_name',
        )


class AddressForm(forms.ModelForm):
    """Register form definition."""

    class Meta:
        model = Address
        fields = '__all__'
