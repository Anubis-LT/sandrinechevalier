from django.contrib import admin

from .models import Account


@admin.register(Account)
class UserModelAdmin(admin.ModelAdmin):
    fields = ('email', 'first_name', 'last_name')
    readonly_fields = ['email']
    list_display = ('email', 'first_name', 'last_name',
                    'last_login')
