from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin

from django.db import models
from django.utils.translation import ugettext_lazy as _


class MyUserManager(BaseUserManager):
    """
    A custom user manager to deal with emails as unique identifiers for auth
    instead of usernames. The default that's used is "UserManager"
    """

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, **extra_fields)


class Account(AbstractBaseUser, PermissionsMixin):
    """Custom User model definition for email identification use."""
    email = models.EmailField(unique=True, null=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    USERNAME_FIELD = 'email'
    objects = MyUserManager()

    def __str__(self):
        return str(self.id) + ' - ' + self.first_name + ' ' + self.last_name

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    class Meta:
        verbose_name = 'Email', 'first_name'


class Address(models.Model):
    users = models.ForeignKey(Account, on_delete=models.CASCADE)
    address = models.CharField(_('address'), max_length=50)
    address2 = models.CharField(_('address more'), max_length=50, blank=True)
    zipcode = models.CharField(_('zipcode'), max_length=5)
    city = models.CharField(_('city'), max_length=60)
    country = models.CharField(_('country'), max_length=50)
    phone1 = models.CharField(_('mobile'), max_length=15, blank=True)
    phone2 = models.CharField(_('fix'), max_length=15, blank=True)
    delivery_address = models.BooleanField(_('address delivery'), default=False)

    class Meta:
        verbose_name = 'Address'
        verbose_name_plural = 'Address'
        unique_together = (('users', 'delivery_address'),)
        indexes = [
            models.Index(fields=['users', 'delivery_address']),
        ]

    def __str__(self):
        return self.address
